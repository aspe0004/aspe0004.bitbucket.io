/// <reference path="gameItem.ts" />

class Car extends GameItem {

    /**
     * constructor for car class
     * @param {name} - Name of the element
     * @param {speed} - Speed of the car
     * @param {id} - ID of car
     * @param {xPosition} - xPos
     * @param {yPosition} - yPos
     */
    constructor(name: string, speed: number, id: string, xPosition: number = 0, yPosition: number = 0) {
        super(name, speed, id, xPosition, yPosition);
    }

    /**
    * Function to move the car to the right randomized
    */
    public move(): void {
        console.log(this._xPos + " " + this._speed + this._name + this._id);
        this._xPos -= Math.floor((Math.random() * this._speed) + 50);;
        document.getElementById(`${this._id}`).style.transition = "all 1.3s";
    }

    /**
     * Function to hide the car from the dom
     */
    public remove(): void{
        document.getElementById(`${this._id}`).style.visibility = "hidden";
        this._xPos = 0;
        document.getElementById(`${this._id}`).style.transition = "all 0.5s";
        this._element.style.transform = `translate(0px, 0px)`;
    }
     /**
     * Funtion to show the car in the dom
     */
    public show(): void{
        document.getElementById(`${this._id}`).style.visibility = "visible";
        this._xPos = 0;
        document.getElementById(`${this._id}`).style.transition = "all 0.5s";
        this._element.style.transform = `translate(0px, 0px)`;
    }
}