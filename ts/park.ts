/// <reference path="gameItem.ts" />

class Park extends GameItem {

    /**
    * constructor for park class
    * @param {string} - name
    * @param {number} - xPosition
    * @param {number} - yPosition
    */
    constructor(name: string, xPosition: number = 0, yPosition: string = '') {
        super(name, xPosition, yPosition);
    }

    /**
    * Function to draw parking spot.
    * @param {HTMLElement} - container
    */
    public draw(container: HTMLElement): void {
        //create div
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = `${this._name}`;

        //p
        const p = document.createElement('p');
        p.innerHTML = 'Park here';
        p.id = 'parking'
        
        //append elements
        this._element.appendChild(p);
        container.appendChild(this._element);
    }

    
}