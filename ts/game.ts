class Game {
  private _element: HTMLElement = document.getElementById('container');
  private _car1: Car;
  private _car2: Car;
  private _car3: Car;
  private _carcount: number;
  private _park: Park;
  private _scoreboard: Score;

  /**
   * constructor for game class
   */
  constructor() {
    // items initialize
    this._car1 = new Car('car', 100, 'car1');
    this._car2 = new Car('car', 200, 'car2');
    this._car3 = new Car('car', 300, 'car3');
    this._park = new Park('park');
    this._scoreboard = new Score('score');
    this._carcount = 0;

    //keydown handler
    window.addEventListener('keydown', this.keyDownHandler);

    this.draw();
  }

  /**
   * Function to see if car has succesfully parked or not.
   */
  public parked(): void {
    // define boundingclientrectangle for collision detection.
      const parkSpot = document.getElementById('park').getBoundingClientRect();
      const carBox1 = document.getElementById('car1').getBoundingClientRect();
      const carBox2 = document.getElementById('car2').getBoundingClientRect();
      const carBox3 = document.getElementById('car3').getBoundingClientRect();

    //check if car drove too far
    if (carBox1.left <= parkSpot.left || carBox2.left <= parkSpot.left || carBox3.left <= parkSpot.left) {
      alert("Try again!");
      this.reset();
    } // else check if car is parked
    else if (carBox1.left >= parkSpot.left && carBox1.right <= parkSpot.right ) {
      this.scoreSound();
      this._car1.remove();
      this._scoreboard.addScore();
      this._carcount++;
      this._car2.show();
    } else if (carBox2.left >= parkSpot.left && carBox2.right <= parkSpot.right) {
      this.scoreSound();
      this._car2.remove();
      this._scoreboard.addScore();
      this._carcount++;
      this._car3.show();
    } else if (carBox3.left >= parkSpot.left && carBox3.right <= parkSpot.right) {
      this.winSound();
      this._car3.remove();
      this._scoreboard.addScore();
      alert("Finished the game!")
      this.reset();
    } 
    else {
        console.log('no collision');
    }
  }

  /**
   * Function to draw the objects
   */
  public draw(): void {
    this._car1.draw(this._element);
    this._car2.draw(this._element);
    this._car3.draw(this._element);
    this._park.draw(this._element);
    this._scoreboard.draw(this._element);
    this._car2.remove();
    this._car3.remove();
  }

  /**
   * Reset every object
   */
  public reset(): void{
    this._car1.remove();
    this._car2.remove();
    this._car3.remove();
    this._car1.show();
    this._scoreboard.resetScore();
    this._carcount = 0;
  }

  /**
   * Update the objects
   */
  public update(): void {
    //check if car is parked
    this.parked();

    //choose which car the player is currently using
    if(this._carcount == 1){
      this._car2.update();
    } else if (this._carcount == 2) {
      this._car3.update();
    } else {
    this._car1.update();
    }

    //update to parking spot and scoreboard
    this._park.update();
    this._scoreboard.update();
  }

  /**
   * Play ding sound
   */
  public scoreSound(): void{
    var audio = <HTMLAudioElement>document.getElementById("ding");
    audio.currentTime = 0;
    audio.play();
  }

  /**
   * Play ding sound
   */
  public winSound(): void{
    var audio = <HTMLAudioElement>document.getElementById("win");
    audio.currentTime = 0;
    audio.play();
  }

  /**
   * Function to handle the keyboard event
   * @param {KeyboardEvent} - event
   */
  public keyDownHandler = (e: KeyboardEvent): void => {
    if (e.keyCode === 32) {

      if(this._carcount == 1){
        this._car2.move();
        }else if (this._carcount == 2) {
        this._car3.move();
        } else{
          this._car1.move();
        }
      this.update();
    }
  }
}