class GameItem {
    constructor(name, speed = 0, id = "", xPosition = 0, yPosition = 0) {
        this._name = name;
        this._speed = speed;
        this._xPos = xPosition;
        this._yPos = yPosition;
        this._id = id;
    }
    set xPos(xPosition) {
        this._xPos = xPosition;
    }
    set yPos(yPosition) {
        this._yPos = yPosition;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._id;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/images/${this._id}.png`;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
    update() {
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }
}
class Car extends GameItem {
    constructor(name, speed, id, xPosition = 0, yPosition = 0) {
        super(name, speed, id, xPosition, yPosition);
    }
    move() {
        console.log(this._xPos + " " + this._speed + this._name + this._id);
        this._xPos -= Math.floor((Math.random() * this._speed) + 50);
        ;
        document.getElementById(`${this._id}`).style.transition = "all 1.3s";
    }
    remove() {
        document.getElementById(`${this._id}`).style.visibility = "hidden";
        this._xPos = 0;
        document.getElementById(`${this._id}`).style.transition = "all 0.5s";
        this._element.style.transform = `translate(0px, 0px)`;
    }
    show() {
        document.getElementById(`${this._id}`).style.visibility = "visible";
        this._xPos = 0;
        document.getElementById(`${this._id}`).style.transition = "all 0.5s";
        this._element.style.transform = `translate(0px, 0px)`;
    }
}
class Game {
    constructor() {
        this._element = document.getElementById('container');
        this.keyDownHandler = (e) => {
            if (e.keyCode === 32) {
                if (this._carcount == 1) {
                    this._car2.move();
                }
                else if (this._carcount == 2) {
                    this._car3.move();
                }
                else {
                    this._car1.move();
                }
                this.update();
            }
        };
        this._car1 = new Car('car', 100, 'car1');
        this._car2 = new Car('car', 200, 'car2');
        this._car3 = new Car('car', 300, 'car3');
        this._park = new Park('park');
        this._scoreboard = new Score('score');
        this._carcount = 0;
        window.addEventListener('keydown', this.keyDownHandler);
        this.draw();
    }
    parked() {
        const parkSpot = document.getElementById('park').getBoundingClientRect();
        const carBox1 = document.getElementById('car1').getBoundingClientRect();
        const carBox2 = document.getElementById('car2').getBoundingClientRect();
        const carBox3 = document.getElementById('car3').getBoundingClientRect();
        if (carBox1.left <= parkSpot.left || carBox2.left <= parkSpot.left || carBox3.left <= parkSpot.left) {
            alert("Try again!");
            this.reset();
        }
        else if (carBox1.left >= parkSpot.left && carBox1.right <= parkSpot.right) {
            this.scoreSound();
            this._car1.remove();
            this._scoreboard.addScore();
            this._carcount++;
            this._car2.show();
        }
        else if (carBox2.left >= parkSpot.left && carBox2.right <= parkSpot.right) {
            this.scoreSound();
            this._car2.remove();
            this._scoreboard.addScore();
            this._carcount++;
            this._car3.show();
        }
        else if (carBox3.left >= parkSpot.left && carBox3.right <= parkSpot.right) {
            this.winSound();
            this._car3.remove();
            this._scoreboard.addScore();
            alert("Finished the game!");
            this.reset();
        }
        else {
            console.log('no collision');
        }
    }
    draw() {
        this._car1.draw(this._element);
        this._car2.draw(this._element);
        this._car3.draw(this._element);
        this._park.draw(this._element);
        this._scoreboard.draw(this._element);
        this._car2.remove();
        this._car3.remove();
    }
    reset() {
        this._car1.remove();
        this._car2.remove();
        this._car3.remove();
        this._car1.show();
        this._scoreboard.resetScore();
        this._carcount = 0;
    }
    update() {
        this.parked();
        if (this._carcount == 1) {
            this._car2.update();
        }
        else if (this._carcount == 2) {
            this._car3.update();
        }
        else {
            this._car1.update();
        }
        this._park.update();
        this._scoreboard.update();
    }
    scoreSound() {
        var audio = document.getElementById("ding");
        audio.currentTime = 0;
        audio.play();
    }
    winSound() {
        var audio = document.getElementById("win");
        audio.currentTime = 0;
        audio.play();
    }
}
let app;
(function () {
    let init = function () {
        app = new Game();
    };
    window.addEventListener('load', init);
})();
class Park extends GameItem {
    constructor(name, xPosition = 0, yPosition = '') {
        super(name, xPosition, yPosition);
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = `${this._name}`;
        const p = document.createElement('p');
        p.innerHTML = 'Park here';
        p.id = 'parking';
        this._element.appendChild(p);
        container.appendChild(this._element);
    }
}
class Score extends GameItem {
    constructor(name) {
        super(name);
        this._score = 0;
    }
    get score() {
        return this._score;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        const p = document.createElement('p');
        p.innerHTML = 'Score: ';
        const span = document.createElement('span');
        span.innerHTML = this._score.toString();
        p.appendChild(span);
        this._element.appendChild(p);
        container.appendChild(this._element);
    }
    update() {
        const scoreSpan = this._element.childNodes[0].childNodes[1];
        scoreSpan.innerHTML = this._score.toString();
    }
    addScore() {
        this._score++;
    }
    resetScore() {
        this._score = 0;
    }
}
//# sourceMappingURL=main.js.map