class GameItem {
    //attr
    protected _element: HTMLElement;
    protected _name: string;
    protected _id: string;
    protected _speed: number;
    protected _xPos: number;
    protected _yPos: number;

    /**
     * constructor for gameitem class
     * @param {name} - name of element
     * @param {speed} - speed (car)
     * @param {id} - id
     * @param {xPosition} - xPos    
     * @param {yPosition} - yPos
     */
    constructor(name: string, speed: number = 0, id: string = "", xPosition: number = 0, yPosition: number = 0) {
        this._name = name
        this._speed = speed;
        this._xPos = xPosition;
        this._yPos = yPosition;
        this._id = id;
    }

    public set xPos(xPosition: number) {
        this._xPos = xPosition;
    }

    public set yPos(yPosition: number) {
        this._yPos = yPosition;
    }

    /**
    * draw the GameItem
    * @param {HTMLElement} - container
    */
    public draw(container: HTMLElement): void {
        //create div
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._id;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;

        //create image
        const image = document.createElement('img');
        image.src = `./assets/images/${this._id}.png`;

        //append elements
        this._element.appendChild(image);
        container.appendChild(this._element);
    }

    /**
    * Function to update gameitem in the DOM
    */    
    public update(): void {
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }
    
}